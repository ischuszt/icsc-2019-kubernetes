# Exercise 01 - Hello Docker

The first thing is to test if Docker is working:
```bash
$ docker run hello-world
```

But this is an image already provided by docker ([here](https://hub.docker.com/_/hello-world) if you are interested), so now let's try to run your turn to write your own hello world.

## 1. Build and run your own first container

1. Edit the Docker file and build your image with (don't forget the dot `.` at the end):
  - `docker build -t image:tag .`

2. Run your container binding the container's ports to a specific port using the -p flag:
```bash
$ docker run -d -p 8080:8080 image:tag
```
The `-d` flag just run the container in background and prints the container ID.

3. Now inspect the container, use either:
  - `curl localhost:8080`
  - `docker ps -a`

4. Kill your container
  - `docker kill`

## 2. Push to our CERN gitlab registry

Now we are going to build and push your image to the gitlab registry.
I set up a repo for you called `repo-cluster-XX`, where `XX` is your student ID. You need to build your image with the following pattern
- `gitlab-registry.cern.ch/icsc-2019/repo-cluster-XX/image-name:tag`

1. Build following the gitlab-registry CERN pattern:
```bash
$ docker build -t gitlab-registry.cern.ch/icsc-2019/repo-cluster-XX/image-name:tag .
```

2. Push your image:
```bash
$ docker push gitlab-registry.cern.ch/icsc-2019/repo-cluster-XX/image-name:tag
```

## 3. Environmental message

1. Modify the the script to serve as a message the content of an environmental variable
2. Modify the Dockerfile to set an environmental variable
  - Hint: `ENV` statement
