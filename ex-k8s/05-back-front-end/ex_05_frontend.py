import os
import platform

from flask_bootstrap import Bootstrap
from flask import Flask, render_template

app = Flask(__name__)
app.env = 'development'

bootstrap = Bootstrap(app)


@app.route('/', methods=['GET', 'POST'])
def index():
    msg = #....

    return render_template('index.html',
                           node=platform.node(),
                           message=msg,
                           platform=platform.platform())


if __name__ == '__main__':
    print('[INFO] Starting flask server app {}...'.format(app))
    print('[INFO] New message: {}'.format(os.environ.get('MESSAGE', "env variable 'MESSAGE' not set")))
    port = int(os.environ.get('PORT', 8080))
    app.run(host='0.0.0.0', port=port, debug=True)
    print('[INFO] Exiting...')
