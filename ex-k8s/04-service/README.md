# Exercise 04 - The (Micro-)Service

The Service exposes your Deployment to the cluster.

## 1. Create a Service

1. The deployment is already prepared for you, create it with:
```bash
$ kubectl create -f ex-04-deployment.yaml
```
2. Example. The following specification creates a Service named "demo" which targets TCP port 8080 on any Pod with the "app=my-demo" label:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: demo
spec:
  type: LoadBalancer
  selector:
    app: my-demo
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
```

3. Now edit the file `ex-04-service.yaml` and create your own Service for your deployed Pods.

4. The Kubernetes Ingress resource is a little bit beyond the scope of the lecture, so I already created everything for you and you can use it as a black box (but if you are already familiar with the concepts there will be an exercise dedicated to this). In short, what Ingress does is to expose your Service outside of the Kubernetes cluster. Therefore run:
```bash
$ docker create -f ex-04-ingress.yaml
```
And then you will be able to access it from within the CERN network. Open your browser and go to the following URL (replace `XX` with the cluster ID assigned to you):
  - Visit: [http://icsc-cluster-XX.cern.ch](http://icsc-cluster-XX.cern.ch)
  - Try refreshing the page and you will see that each time the serving pod is different.
  - What are your conclusions about what is happening?
