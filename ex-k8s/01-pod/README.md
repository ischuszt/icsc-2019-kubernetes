# Exercise 01 - Playing with Pods

## Tutorial - Deploy your first Pod
Being that this might be your first time, we'll do this first one together.

1. First we'll examine the content of the `ex-01-nginx-pod.yaml` resource file. If you are unfamiliar with the `yaml` format you check this quick guide [here](https://learnxinyminutes.com/docs/yaml/).
```bash
$ cat ex-01-nginx-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: ex-01-nginx-pod
spec:
  containers:
  - name: cnt-nginx
    image: nginx
```
The image property of a container supports the same syntax as the docker command does. If you are wondering, it is coming from [here](https://hub.docker.com/search/?q=&type=image&image_filter=official).


2. Now we'll create a pod with `kubectl create -f`:
```bash
kubectl create -f ex-01-nginx-pod.yaml
pod "ex-01-nginx-pod" created
```

3. Now we'll check whether the pod it is in the running state with `kubectl get pods`:
```bash
$ kubectl get pods
NAME              READY     STATUS    RESTARTS   AGE
ex-01-nginx-pod   1/1       Running   0          21m
```

4. Since we created a Pod with an nginx image we can forward the pod port to where nginx is serving to the localhost with a simple `kubectl port-forward` command:
```bash
$ kubectl port-forward pod-nginx-ex-01 8080:80  
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
Handling connection for 8080
```
If you want you can hit the localhost and get the nginx welcoming page. I let you handle moving the forwarding command to the background.

```bash
$ curl localhost:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

5. We can also inspect the stdout of a Pod with `kubectl logs`:
```bash
$ kubectl logs pod-nginx-ex-01
127.0.0.1 - - [01/Mar/2019:11:44:18 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.29.0" "-"
```

6. Finally, we can also access the Kuberenets dashboard. Due to a bug in the platform used for the exercises we have to use the `kubectl proxy` command to access the dashboard, since this might be intimidating at first I've prepared an helper command for that. Just run the following and follow the instructions on your terminal:

```bash
$ ./helper.sh dashboard
[INFO] To access the dashboard visit the following URL from your own browser:
XX.cern.ch:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

[INFO] Use the following token in the login page (select the 'Token' option):

eyJh...6lg

[INFO] Running 'kubectl proxy', use CTRL-C to exit:
$ kubectl proxy --address='0.0.0.0' --accept-hosts='.*'
Starting to serve on [::]:8001
```

7. We are now ready to delete the Pod with `kubectl delete` (remember that you have auto-completion):
```bash
$ kubectl delete pod ex-01-nginx-pod
pod "ex-01-nginx-pod" deleted
```

## Exercise - Build, push and deploy your own Pod

To help you build and push your own image to the docker registry

### 0. Docker build and push
I created an helper function to help you out with this task, but feel fee to just use it as documentation:
```bash
$ ./helper.sh docker 1
[INFO] Already logged in gitlab-registry.cern.ch
[INFO] Running docker build and docker push...
$ docker build -t gitlab-registry.cern.ch/icsc-2019/repo-cluster-86/ex-01-app:1 .
$ docker push gitlab-registry.cern.ch/icsc-2019/repo-cluster-86/ex-01-app:1
[INFO] New image available at: 'gitlab-registry.cern.ch/icsc-2019/repo-cluster-086/ex-01-app:1'
```

### 1. Create your own Pod
Edit the `ex-01-app.yaml` file and then create your Pod with:
```bash
$ kubectl create -f ex-01-app.yaml
```

### 2. More dynamic message
Right now the message is hard-coded inside the `ex-01-app.py` script, which means that if you want to change it you need edit the script, build a new container image, push it to the registry. Can you find an easy yet clever way to pass a message to your script container image (and to your Pod)?

(We will keep improving on this point in one of the next exercises).

### 3. Killing Pods
1. What happens if you kill or delete a Pod? A few useful commands:
  - `kubectl get pods`
  - `kubectl logs <pod id> --previous`
2. In the lecture we talked about the concept of a graceful shutdown, a few hints:
  - The last logging message is never executed, can you modify the `ex-01-app.py` in such a way to make it log that it is about to (gracefully) exit?
  - Check the `terminationGracePeriodSeconds` spec
  - Extra: Kubernetes offers a `preStop` hook which is called immediately before a container is terminated

### Conclusion
Take home message: Pods are the basic schedule of all unit, but they don't have any notion of persistence, that is if they die or they're killed or the node goes down, there is nothing in the framework to recreate them. That is what a ReplicaSet is for.
