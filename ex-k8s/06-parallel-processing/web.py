import os

from flask import Flask
from flask_bootstrap import Bootstrap

import redis
from rq import Queue, Connection
from flask import render_template, jsonify, request
from tasks import create_task

import rq_dashboard

app = Flask(__name__)
app.config.from_object(rq_dashboard.default_settings)

app.config['REDIS_URL'] = 'redis://ex-07-redis-master:6379'

app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")

#app_settings = os.getenv('APP_SETTINGS')
#app.config.from_object(app_settings)


bootstrap = Bootstrap(app)



@app.route('/', methods=['GET'])
def home():
    return render_template('main/home.html')


@app.route('/tasks', methods=['POST'])
def run_task():
    task_type = request.form['type']
    with Connection(redis.from_url(app.config['REDIS_URL'])):
        q = Queue()
        print('Queue: {}'.format(q))
        task = q.enqueue(create_task, task_type)
    response_object = {
        'status': 'success',
        'data': {
            'task_id': task.get_id()
        }
    }
    return jsonify(response_object), 202

@app.route('/tasks/<task_id>', methods=['GET'])
def get_status(task_id):
    with Connection(redis.from_url(app.config['REDIS_URL'])):
        q = Queue()
        task = q.fetch_job(task_id)
    if task:
        response_object = {
            'status': 'success',
            'data': {
                'task_id': task.get_id(),
                'task_status': task.get_status(),
                'task_result': task.result,
            }
        }
    else:
        response_object = {'status': 'error'}
    return jsonify(response_object)


if __name__ == '__main__':
    print('[INFO] Starting flask server app {}...'.format(app))
    port = int(os.environ.get('PORT', 8080))
    app.run(host='0.0.0.0', port=port, debug=True)

