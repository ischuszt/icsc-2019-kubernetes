# Exercise 06 - Workers scalability for parallel processing

## 1. Scalability measurement

Create Redis in HA:
```bash
$ kubectl create -f redis-master.yaml
$ kubectl create -f redis-slave.yaml
```
