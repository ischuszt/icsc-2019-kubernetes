#!/bin/bash

current=$PWD

cd ../../clusters/
for cid in $(seq -f "%02g" 21 35); do
  mkdir cluster-${cid}
  cd cluster-${cid}
  $(openstack coe cluster config icsc-cluster-${cid})
  mv config config.template
  cd ..
done

cd ${current}
