#!/bin/bash

for cid in $(seq -f "%02g" 22 35); do

  echo "$ openstack coe cluster create icsc-cluster-${cid} --keypair lxplus  --cluster-template kubernetes --node-count 2"
  openstack coe cluster create icsc-cluster-${cid} --keypair lxplus  --cluster-template kubernetes --node-count 2
  sleep 1
done
